
import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    image: {
      width: 300, // Ajustez la largeur de l'image selon vos besoins
      height: 300, // Ajustez la hauteur de l'image selon vos besoins
      resizeMode: 'contain', // Ajustez le mode de redimensionnement de l'image
    },
    text: {
      fontSize: 20, 
      marginTop: 20, 
      color: "white",
      fontStyle: "italic",
      fontFamily: "Snell Roundhand",
    },
    backgroundImage :{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      resizeMode: 'cover',
      width: 'auto',
      height: 'auto'},
  
      buttonRow: {
        flexDirection: 'row',
        
      },
});

export default styles;