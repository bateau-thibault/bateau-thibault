import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    text: {
      fontSize: 20,
      marginTop: 20,
    },
    productItem: {
      flexDirection: 'row',
      alignItems: 'center',
      padding: 10,
      borderBottomWidth: 1,
      borderColor: '#ccc',
    },
    productImageContainer: {
      borderRadius: 60, 
      overflow: 'hidden',
    },
    productImage: {
      width: 30, 
      height: 30,
    },
    productInfo: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between', 
      alignItems: 'center',
      margin: 10, 
    },
    productName: {
      flex: 1, 
      marginRight: 10, 
    },
    productPrice: {
      fontWeight: 'bold', 
    },
    backgroundImage :{flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      resizeMode: 'cover',
      width: 'auto',
      height: 'auto'},
  
      buttonRow: {
        flexDirection: 'row',
        
      },
  });


  export default styles;
