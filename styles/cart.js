import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: {
    fontSize: 20,
    marginTop: 30,
    fontStyle: "italic"
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 15,
  },
  headerText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 20,
  },
  emptyCart: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  cartItems: {
    flex: 1,
    padding: 15,
  },
  cartItem: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    height: 70,
    borderBottomWidth: 1,
    borderColor: '#ccc',
  },
  totalPrixText: {
    fontWeight: "bold",
    fontSize: 18,
    color: 'black',
    marginLeft: 'auto', 
    marginRight: 20, 
    fontStyle:"italic"
  },
  productInfo: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 10,
  },
  productPrice: {
    fontWeight: 'bold',
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: 'cover',
    width: 'auto',
    height: 'auto',
  },
  buttonRow: {
    flexDirection: 'row',
  },
  productImage: {
    width: 30,
    height: 30,
  },
  productImageContainer: {
    borderRadius: 50,
    overflow: 'hidden',
  },
  totalContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0, // Positionner à droite
    width: '100%',
    backgroundColor: 'transparent',
    padding: 10,
    borderTopWidth: 1,
    borderColor: '#ccc',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default styles;
