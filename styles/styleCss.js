// components/RestaurantsStyles.ts
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  safeView: {
    flex: 1,
  },
  container: {
    flex: 1,
  },
  contentContainer: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 100,
  },
  namecom: {
    textAlign: 'center',
    margin: 10,
  },
  
  descriptioncom: {
    textAlign: 'center',
    margin: 10,
  },
  buttonContainer: {
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  caption: {
    textAlign: 'center',
  },

  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  customButton: {
    flexDirection: 'row',
    alignContent: 'center',
    paddingLeft: 10,
    alignItems: 'center',
    margin: 10,
    width: 300,
    height: 50,
    backgroundColor: '#8c939c',
    borderRadius: 10,
    backgroundColor: "rgba(0, 0, 0, 0.3)"
  },
  button2: {
    flexDirection: 'row',
    alignContent: 'center',
    paddingLeft: 10,
    alignItems: 'center',
    margin: 10,
    width: 140,
    height: 50,
    backgroundColor: '#8c939c',
    borderRadius: 10,
    backgroundColor: "rgba(0, 0, 0, 0.3)"
  },
  buttonImage: {
    width: 35,
    height: 35,
  },
  buttonText: {
    color: 'white',
    marginLeft: 10,
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: 'cover',
    width: 'auto',
    height: 'auto',
  },
  contenu: {
    textAlign: 'center',
    margin: 10,
    alignItems: 'center',

  },
});

export default styles;
