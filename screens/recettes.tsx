import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Recettes from '../components/Recettes';
import RecetteDetails from '../components/RecetteDetails';

const Stack = createStackNavigator();

const RecetteScreen = () => (
  <Stack.Navigator screenOptions={{
    headerShown: false
  }}>
    <Stack.Screen name="Recettes" component={Recettes}
      options={
        { 
          headerShown: false,
        }
      }
      />
    <Stack.Screen name="RecetteDetails" 
    // @ts-ignore
    component={RecetteDetails}
      options={
        { 
          headerShown: false,
        }
      }
       />
  </Stack.Navigator>
);

export default RecetteScreen;
