import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import Restaurants from '../components/Restaurants';
import RestaurantDetails from '../components/RestaurantDetails';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const RestaurantScreen = () => (
  <Stack.Navigator screenOptions={{
    headerShown: false
  }}>
    
  <Stack.Screen name="Restaurants" component={Restaurants} />
  <Stack.Screen name="RestaurantDetails" component={RestaurantDetails} />
</Stack.Navigator>
);
export default RestaurantScreen;
