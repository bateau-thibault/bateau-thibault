import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Cart from '../components/Cart';
import CustomHeader from '../components/CustomHeader';

const Stack = createStackNavigator();

const CartScreen = () => (
    <Stack.Navigator screenOptions={{
        headerShown: false
    }}>
    <Stack.Screen name="Cart" component={Cart}  options={
        { 
          headerShown: false,
        }
      }></Stack.Screen>
    </Stack.Navigator>
);

export default CartScreen;
