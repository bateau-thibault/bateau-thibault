import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Contacts from '../components/Contacts';

const Stack = createStackNavigator();

const ContactsScreen = () => (
  
  <Stack.Navigator screenOptions={{headerShown: false}}>
    <Stack.Screen name="Contacts" component={Contacts} />
  </Stack.Navigator>
);

export default ContactsScreen;
