import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Bateaux from '../components/Bateaux';
import BateauDetails from '../components/BateauDetails';

const Stack = createStackNavigator();

const BateauScreen = () => (
  <Stack.Navigator screenOptions={{
    headerShown: false
  }}>
    <Stack.Screen name="Bateaux" component={Bateaux}
      options={
        { 
          headerShown: false,
        }
      }
      />
      {/* @ts-ignore */}
    <Stack.Screen name="BateauDetails" component={BateauDetails}
      options={
        { 
          headerShown: false,
        }
      }
       />
  </Stack.Navigator>
);

export default BateauScreen;
