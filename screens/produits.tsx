import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Produits from '../components/Produits';
import ProduitsDetails from '../components/ProduitsDetails';
import CartScreen from './cart';

const Stack = createStackNavigator();

const ProduitsScreen = () => (
  
  <Stack.Navigator screenOptions={{
    headerShown: false
  }}>
    <Stack.Screen name="Produits" component={Produits} />
    <Stack.Screen name="ProduitsDetails" component={ProduitsDetails} />
    <Stack.Screen 
    name="cart" component={CartScreen}
    options={
      { 
        headerShown: false,
      }
    }
  />
  </Stack.Navigator>
);


export default ProduitsScreen;
