import React from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import styles from '../styles/styleCss';
import { useNavigation, ParamListBase } from '@react-navigation/native';
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import CustomHeader from './CustomHeader';

const Produits = () => {
  const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>();

  const navigateToProduitDetails = (produitName: string, title?: string) => {
    navigation.navigate('ProduitsDetails', { produitName, title });
  };

  return (
    <>
      <CustomHeader />

      <View style={styles.container}>
        <ImageBackground
          source={require('../assets/images/background.png')} style={styles.backgroundImage}
        >
          <View>
            <Text style={styles.descriptioncom}>Tous les Produits : </Text>
          </View>

          <View style={styles.buttonContainer}>

            <TouchableOpacity style={styles.button2} onPress={() => navigateToProduitDetails("Poissons", "Poissons")}>
              <Text style={styles.buttonText}>Poissons</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.button2} onPress={() => navigateToProduitDetails("Coquillages", "Coquillages")}>
              <Text style={styles.buttonText}>Coquillages</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.button2} onPress={() => navigateToProduitDetails("Crustacés", "Crustacés")}>
              <Text style={styles.buttonText}>Crustacés</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.button2} onPress={() => navigateToProduitDetails("Promotions", "Promotions")}>
              <Text style={styles.buttonText}>Promotions</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
    </>
  );
};

export default Produits;
