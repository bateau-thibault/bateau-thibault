import React from "react";
import { View, Text, Image, StyleSheet, ImageBackground } from "react-native";
import { RouteProp } from "@react-navigation/native";
import CustomHeader from "./CustomHeader";
import styles from '../styles/restaurantDetails';


export type RootStackParamList = {
  Bateaux: undefined;
  BateauDetails: { bateauName: string };
};

type BateauDetailsRouteProps = {
  route: RouteProp<RootStackParamList, "BateauDetails">;
};

type BateauImages = {
  [key: string]: any;
};

const bateauImages: BateauImages = {
  DeLaBrise: require("../assets/images/deLaBrise.png"),
  GastMicher: require("../assets/images/gastMicher.png"),
  Saphir: require("../assets/images/saphir.png"),
  Aquilon: require("../assets/images/aquilon.png"),
};
const BateauDetails: React.FC<BateauDetailsRouteProps> = ({ route }) => {
  const { bateauName } = route.params;
  const bateauImage = bateauImages[bateauName];

  return (
    <>
      <CustomHeader />
      <View style={styles.container}>
        <ImageBackground
          source={require("../assets/images/background.png")}
          style={styles.backgroundImage}
        >
          <Text style={styles.text}>{bateauName}</Text>
          <Image source={bateauImage} style={styles.image} />
          <Text> XXX YYY ZZZ </Text>

          <View>
            <Text>Qu'il est chaud le Soleil</Text>
            <Text>Quand nous sommes en vacances</Text>
            <Text>Y a d'la joe, des hirondelles</Text>
            <Text>C'est le Sud de la France</Text>
            <Text>Papa bricole au garage</Text>
            <Text>Maman lit dans la chaise longue</Text>
            <Text>Dans ce jolie paysage</Text>
            <Text>Moi, je me balade en tongs</Text>
            <Text>{"\n"}Que de bonheur !</Text>
            <Text>Que de bonheur !</Text>
          </View>

        </ImageBackground>
      </View>
    </>
  );
};

export default BateauDetails;
