import React from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, ImageBackground } from 'react-native'; import styles from '../styles/styleCss';
import { useNavigation, ParamListBase } from '@react-navigation/native';
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import CustomHeader from './CustomHeader';

const Restaurants = () => {
  const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>();
  const navigateToRestaurantDetails = (restaurantName: string, title?: string) => {
    navigation.navigate('RestaurantDetails', { restaurantName, title });
  };


  return (
    <>
      <CustomHeader />
      <View style={styles.container}>
        <ImageBackground source={require('../assets/images/background.png')} style={styles.backgroundImage}>
          <View>
            <Text style={styles.namecom}>Restaurants partenaires</Text>
            <Text style={styles.descriptioncom}>Tous les restaurants partenaires avec le bateau de Thibault.</Text>
          </View>

          <View style={styles.buttonContainer}>
            <View style={styles.row}>
              <TouchableOpacity style={styles.button2} onPress={() => navigateToRestaurantDetails("desGascons", "Des Gascons")}>
                <Text style={styles.buttonText}>Bistrot des Gascons</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.button2} onPress={() => navigateToRestaurantDetails("fousDeLIle", "Fous De LIle")}>
                <Text style={styles.buttonText}>Les fous de l'île</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.row}>
              <TouchableOpacity style={styles.button2} onPress={() => navigateToRestaurantDetails("bistrotLandais", "Bistrot Landais")}>
                <Text style={styles.buttonText}>Bistrot Landais</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.button2} onPress={() => navigateToRestaurantDetails("villa9Trois", "Villa9 Trois")}>
                <Text style={styles.buttonText}>Villa 9-Trois</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </View>

    </>
  );
};

export default Restaurants;
