import React from 'react';
import { View, Text, Image, StyleSheet, ImageBackground } from 'react-native';
import { RouteProp } from '@react-navigation/native';
import CustomHeader from './CustomHeader';
import restaurantDetailsstyles from '../styles/restaurantDetails';


export type RootStackParamList = {
  Restaurants: undefined;
  RestaurantDetails: { restaurantName: string };
};

type RestaurantDetailsRouteProps = {
  route: RouteProp<RootStackParamList, 'RestaurantDetails'>;
};

type RestaurantImages = {
  [key: string]: any;
};

const restaurantImages: RestaurantImages = {
  "fousDeLIle": require('../assets/images/fousDeLIle.png'),
  "desGascons": require('../assets/images/desGascons.png'),
  "bistrotLandais": require('../assets/images/bistrotLandais.png'),
  "villa9Trois": require('../assets/images/villa9Trois.png'),
  "duSommelier": require('../assets/images/duSommelier.png'),
};

const RestaurantDetails: React.FC<RestaurantDetailsRouteProps> = ({ route }) => {
  const { restaurantName } = route.params;
  const restaurantImage = restaurantImages[restaurantName];

  return (
<>
      <CustomHeader />
      <View style={restaurantDetailsstyles.container}>
        <ImageBackground
          source={require("../assets/images/background.png")}
          style={restaurantDetailsstyles.backgroundImage}
        >
          <Text style={restaurantDetailsstyles.text}>{restaurantName}</Text>
          <Image source={restaurantImage} style={restaurantDetailsstyles.image} />
          <Text> XXX YYY ZZZ </Text>

          <View>
            <Text>Qu'il est chaud le Soleil</Text>
            <Text>Quand nous sommes en vacances</Text>
            <Text>Y a d'la joe, des hirondelles</Text>
            <Text>C'est le Sud de la France</Text>
            <Text>Papa bricole au garage</Text>
            <Text>Maman lit dans la chaise longue</Text>
            <Text>Dans ce jolie paysage</Text>
            <Text>Moi, je me balade en tongs</Text>
            <Text>{"\n"}Que de bonheur !</Text>
            <Text>Que de bonheur !</Text>
          </View>

          {}
        </ImageBackground>
      </View>
    </>
  );
};

export default RestaurantDetails;
