import React from "react";
import { View, Text } from "react-native";
import { useRoute } from "@react-navigation/native";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import { TouchableOpacity } from 'react-native'; // Importez TouchableOpacity depuis react-native
import { useNavigation, ParamListBase } from '@react-navigation/native';
import {NativeStackNavigationProp} from "@react-navigation/native-stack";

const CustomHeader = () => {
  const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>();

  const route = useRoute();

  return (
    <>
            <View style={{
                height:70,
                flexDirection:'row',
                alignItems:'center',
                paddingHorizontal:15,
                backgroundColor: "white",
            }}>
                <View style={{flex:1,flexDirection:'row',alignItems:'center'}}>
                    <Ionicons
                        name="arrow-back"
                        size={45}
                        color="#8c939c"
                        onPress={() => navigation.goBack()}
                    />
                    <View>
                        <Text
                            style={{
                            color: "black",
                            fontWeight: "bold",
                            fontSize: 20,
                            }}
                        >
                            {// @ts-ignore
                            route.params && route.params.title ? route.params.title : route.name}
                        </Text>
                    </View>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('cart')}
                    style={{
                        height:45,
                        width:45,
                        borderWidth:1,
                        backgroundColor: "white",
                        borderRadius: 100,
                        borderColor: "black",
                        alignItems:'center',
                        justifyContent:'center',
                    }}
                >
                    <AntDesign name="shoppingcart" size={24} color="black" />
                </TouchableOpacity>
            </View>
        </>
  );
};

export default CustomHeader;
