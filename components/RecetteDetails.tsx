import React from "react";
import { View, Text, Image, ImageBackground } from "react-native";
import { RouteProp } from "@react-navigation/native";
import CustomHeader from "./CustomHeader";
import styles from '../styles/restaurantDetails';

export type RootStackParamList = {
  Recette: undefined;
  RecetteDetails: { recetteName: string };
};

type RecetteDetailsRouteProps = {
  route: RouteProp<RootStackParamList, "RecetteDetails">;
};

const recettes  = {
    'Homard': {
      name:"Homard en chaud froia",
      photo:"../assets/images/homardRecette.png",
      description:[
        "Faites cuire les homards dans de l'eau bouillante avec du thym, du laurier",
        "du sel et du poivre de Cayenne. Laissez cuire 20 minutes. ",
        "gouttez-les et laissez-les refroidir.",
        "coupez les coffres des homards dans le sens de la longueur.",
        "langez la mayonnaise avec le cognac, le corail et la ciboulette cisel"
      ]
    },
    'St Jacques':{
      name:"saint Jacques",
      photo:"../assets/images/saintJacques.png",
      description:[
        "Faire fondre du beurre avec des chalotes puis ajouter les noix de Saint-Jacques.",
        "Les faire revenir en laissant le milieu translucide puis les retirer du feu.",
        "Ajouter l'ail et le persil dans la poale et laisser cuire quelques secondes.",
        "Bien faire chauffer la poale, puis flamber au Cognac.",
        "Une fois la flamme teinte, ajouter les noix de Saint-Jacques (il ne faut pas les flamber car elles perdraient leur saveur).",
        "guster chaud nature ou accompagnement d'une fondue de poireaux."
     
      ]
    },
    'Bar': {
      name:'Bar rôti au laurier frais',
      photo:"../assets/images/barRecette.png",
      description:[
        "Sur une plaque ou un plat allant au four, disposer  quelques feuilles de laurier frais, verser un filet d'huile d'olive et du gros sel. ",
        "Disposer le bar, puis l'arroser d'un filet d'huile d'olive et mettre un peu de gros sel sur la peau.",
        "Cuire au four pendant 12 min"
      ]
    },
    'Tourteau': {
      name:'Tourteau',
      photo:"../assets/images/tourteau.png",
      description:[
        "Qu'il est chaud le soleil",
        "Quand nous sommes en vacances",
        "Y a d' la joie, des hirondelles",
        "C'est le sud de la France ",
        "Papa bricole au garage",
        "Maman lit dans la chaise longue",
        "Dans ce joli paysage",
        "Moi, je me balade en tongs",
        "Que de bonheur!",
        "Que de bonheur!"
      ]
    },
    'Recette': {
      name:'Tourteau',
      photo:"../assets/images/poulpe.png",
      description:[
        "Qu'il est chaud le soleil",
        "Quand nous sommes en vacances",
        "Y a d' la joie, des hirondelles",
        "C'est le sud de la France ",
        "Papa bricole au garage",
        "Maman lit dans la chaise longue",
        "Dans ce joli paysage",
        "Moi, je me balade en tongs",
        "Que de bonheur!",
        "Que de bonheur!"
      ]
    }
  };
  
const RecetteDetails: React.FC<RecetteDetailsRouteProps> = ({ route }) => {
  const { recetteName } = route.params;

  return (
    <>
      <CustomHeader />
      <View style={styles.container}>
        <ImageBackground
          source={require("../assets/images/background.png")}
          style={styles.backgroundImage}
        >
          <Text style={styles.text}>{recetteName}</Text>
          {/* @ts-ignore */}
          <Image source={recettes[recetteName].photo} style={styles.image} />
          <Text> XXX YYY ZZZ </Text>

          <View>
            {
              // @ts-ignore
              recettes[recetteName].description.map((item: string, index) => (
                <Text key={index}>{item}</Text>
              ))
            }
          </View>

        </ImageBackground>
      </View>
    </>
  );
};

export default RecetteDetails;
