import React from "react";
import { View, Text, Image, StyleSheet, ImageBackground } from "react-native";
import CustomHeader from "./CustomHeader";
import styles from '../styles/restaurantDetails';


const Contacts: React.FC = ({  }) => {

  return (
    <>
      <CustomHeader />
      <View style={styles.container}>
        <ImageBackground
          source={require("../assets/images/background.png")}
          style={styles.backgroundImage}
        >
          <Text style={styles.text}>Contact</Text>
          <Image source={require('../assets/images/TIG.png')} style={styles.image} />
          <Text> XXX YYY ZZZ </Text>

          <View>
            <Text>Qu'il est chaud le Soleil</Text>
            <Text>Quand nous sommes en vacances</Text>
            <Text>Y a d'la joe, des hirondelles</Text>
            <Text>C'est le Sud de la France</Text>
            <Text>Papa bricole au garage</Text>
            <Text>Maman lit dans la chaise longue</Text>
            <Text>Dans ce jolie paysage</Text>
            <Text>Moi, je me balade en tongs</Text>
            <Text>{"\n"}Que de bonheur !</Text>
            <Text>Que de bonheur !</Text>
          </View>

        </ImageBackground>
      </View>
    </>
  );
};

export default Contacts;
