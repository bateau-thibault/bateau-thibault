import React from 'react';
import styles from '../styles/styleCss';
import { View, Text, ImageBackground, TouchableOpacity, Image } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import CustomHeader from './CustomHeader';
import { ParamListBase, useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

export interface IRecettesArray {
  source1: any;
  text1: string;
  source2: any;
  text2: string;
}

const Recettes = () => {
  const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>();

  const navigateToRecetteDetails = (recetteName : string, title?: string) => {
    navigation.navigate('RecetteDetails', { recetteName, title });
  };

  const recettesArray: IRecettesArray[] = [
    {
      source1: require('../assets/images/homardRecette.png'),
      text1: 'Homard',
      source2: require('../assets/images/saintJacques.png'),
      text2: 'St Jacques',
    },
    {
      source1: require('../assets/images/barRecette.png'),
      text1: 'Bar',
      source2: require('../assets/images/tourteau.png'),
      text2: 'Tourteau',
    },
    {
      source1: require('../assets/images/poulpe.png'),
      text1: 'Recette',
      source2: require('../assets/images/poulpe.png'),
      text2: 'Recette',
    }
  ];

  return (
    <SafeAreaView style={styles.safeView}>
      <CustomHeader />
        <ImageBackground source={require('../assets/images/background.png')} style={styles.backgroundImage}>
          <View style={styles.contentContainer}>
            <View>
              <Text style={styles.title}>Nos recettes</Text>
              <Text style={styles.contenu}>Toutes les recettes du bateau de Thibault.</Text>
              <Text style={styles.contenu}>06.63.99.99.78</Text>
              <Text style={styles.contenu}>lebateaudethibault@gmail.com</Text>
              <Text style={styles.contenu}>www.facebook.com/lebateaudethibault</Text>
            </View>

            <View style={styles.buttonContainer}>
              {
                recettesArray.map((item: IRecettesArray, index) => (
                  <View style={styles.row}>
                    <TouchableOpacity style={styles.button2} onPress={() => navigateToRecetteDetails(item.text1,  item.text1)}>
                      <Image source={item.source1} style={styles.buttonImage} />
                      <Text style={styles.buttonText}>{item.text1}</Text>
                    </TouchableOpacity>
          
                    <TouchableOpacity style={styles.button2} onPress={() => navigateToRecetteDetails(item.text2,  item.text2)}>
                      <Image source={item.source2} style={styles.buttonImage} />
                      <Text style={styles.buttonText}>{item.text2}</Text>
                    </TouchableOpacity>
          
                  </View>
                ))
              }
              </View>
            </View>
        </ImageBackground>
    </SafeAreaView>
  );
};

export default Recettes;
