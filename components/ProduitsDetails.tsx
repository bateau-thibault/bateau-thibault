import React, { useContext } from 'react';
import { View, Text, Image, StyleSheet, FlatList, ImageBackground, TouchableOpacity } from 'react-native';
import { RouteProp } from '@react-navigation/native';
import CustomHeader from './CustomHeader';
import { ProduitsContext } from '../contexts/produitsContext';
import styles from '../styles/produitsDetails';

export type RootStackParamList = {
  Produits: undefined;
  ProduitsDetails: { produitName: string };
};

type ProduitsDetailsRouteProps = {
  route: RouteProp<RootStackParamList, 'ProduitsDetails'>;
};

export type ProduitInfo = {
  nom: string;
  prix: number;
  image: any;
  quantite:number;
};

type ProduitMenu = {
  [categorie: string]: ProduitInfo[];
};

const ProduitsMenu: ProduitMenu = {
  "Poissons": [
    { nom: "Saumon", prix: 15, image: require('../assets/images/tourteau.png') , quantite:1 },
    { nom: "Truite", prix: 12, image: require('../assets/images/tourteau.png'), quantite:1 },
    { nom: "Morue", prix: 18, image: require('../assets/images/tourteau.png'),quantite:1 }
  ],
  "Coquillages": [
    { nom: "Moules de pêche", prix: 7, image : require('../assets/images/tourteau.png'), quantite:1},
    { nom: "Boquets cuits", prix: 8 , image : require('../assets/images/tourteau.png'), quantite:1},
    { nom: "Huitres N°2 St Vaast", prix: 9 , image : require('../assets/images/tourteau.png'), quantite:1},
    { nom: "Huitres N°2 OR St Vaast", prix: 12 , image : require('../assets/images/tourteau.png'), quantite:1},
    { nom: "Huitres N°2 St Vaast", prix: 19 , image : require('../assets/images/tourteau.png'), quantite:1},
    { nom: "Huitres N°2 OR Vaast", prix: 24 , image : require('../assets/images/tourteau.png'), quantite:1},
    { nom: "Boquets cuits", prix: 38 , image : require('../assets/images/tourteau.png'), quantite:1},
    { nom: "Huitres N°2 St Vaast", prix: 48 , image : require('../assets/images/tourteau.png'), quantite:1},
  ],
  "Crustacés": [
    { nom: "Crevettes", prix: 10, image : require('../assets/images/tourteau.png'), quantite:1 },
    { nom: "Homard", prix: 25, image : require('../assets/images/tourteau.png'), quantite:1},
    { nom: "Crabe", prix: 18, image : require('../assets/images/tourteau.png'), quantite:1 }
  ],
  "Promotions": [
    { nom: "Promo 1", prix: 5, image : require('../assets/images/tourteau.png'), quantite:1 },
    { nom: "Promo 2", prix: 7, image : require('../assets/images/tourteau.png'), quantite:1 },
    { nom: "Promo 3", prix: 9, image : require('../assets/images/tourteau.png'), quantite:1 }
  ]
};



const ProduitsDetails: React.FC<ProduitsDetailsRouteProps> = ({ route }) => {
  const { produitName } = route.params;
  const produitsDeLaCategorie = ProduitsMenu[produitName];
  const { addProduit } = useContext(ProduitsContext); // Utilisez le hook pour accéder au contexte

  return (
    <>    
    <CustomHeader />

      <View style={styles.container}>
      <ImageBackground
      source={require('../assets/images/background.png')}
      style={styles.backgroundImage}
    >
        <Text style={styles.text}>Choisissez vos produits</Text>
        <FlatList
          data={produitsDeLaCategorie}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <View style={styles.productItem}>

              <View style={styles.productImageContainer}>
                <Image source={item.image} style={styles.productImage} />
              </View>

              <View style={styles.productInfo}>

              <TouchableOpacity onPress={() => addProduit(item)}>
                <Text style={styles.productName}>{item.nom}</Text>
              </TouchableOpacity>  

              <Text style={styles.productPrice}>{item.prix} euros</Text>                
              
              </View>
            </View>
          )}
        />
      </ImageBackground>
      </View>

    </>
  );
};

export default ProduitsDetails;