import React, { useContext, useEffect, useState } from "react";
import { View, Text, Image, StyleSheet, ImageBackground, FlatList, TouchableOpacity, Button } from "react-native";
import styles from '../styles/cart';
import { ProduitsContext } from "../contexts/produitsContext";
import CustomHeader from "./CustomHeader";

const Cart = () => {
  const { getProduits, removeProduit } = useContext(ProduitsContext);
  const [produits, setProduits] = useState(getProduits()); 

  const totalPrix = produits.reduce((total, produit) => total + produit.prix * produit.quantite, 0);

  const handleRemoveProduit = (produit : any) => {
    removeProduit(produit);
    setProduits(prevProduits => prevProduits.filter(p => p.nom !== produit.nom));
  }

  const handleValider = () => {
    alert('Envoyer votre commande? \nEnvoyer votre commande de ' + totalPrix + ' € à Thibault ?');
    alert('Votre commande a été envoyée avec succès');
  }

  return (
    <>
      <CustomHeader />
      <ImageBackground
        source={require('../assets/images/background.png')}
        style={styles.backgroundImage}
      >
        {produits.length === 0 ? (
          <View style={styles.emptyCart}>
            <Text>Votre panier est vide</Text>
          </View>
        ) :
          (
            <View style={styles.container}>
              <Text style={styles.text}>Modifiez la quantité en tapant sur chaque produit</Text>
              <FlatList
                data={produits}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <View style={styles.cartItem}>
                    <View style={styles.productImageContainer}>
                      <Image source={item.image} style={styles.productImage} />
                    </View>
                    <View style={styles.productInfo}>
                      <TouchableOpacity onPress={() => handleRemoveProduit(item)}>
                        <Text>{item.nom}</Text>
                      </TouchableOpacity>

                      <Text style={styles.productPrice}>
                        {item.quantite} x {item.prix} : {item.quantite * item.prix} euros
                      </Text>
                    </View>
                  </View>
                )}
              />
              <View style={styles.totalContainer}>
                <Text style={styles.totalPrixText}>Total: {totalPrix} €</Text>
                <Button title="Valider" onPress={handleValider} />
              </View>
            </View>
          )}
      </ImageBackground>
    </>
  );
};

export default Cart;
