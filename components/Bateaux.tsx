// components/Bateaux.tsx
import React, { useEffect } from 'react';
import { View, Text, Button, Image, TouchableOpacity, ImageBackground } from 'react-native';
import styles from '../styles/styleCss';
import { useNavigation, ParamListBase } from '@react-navigation/native';
import {NativeStackNavigationProp} from "@react-navigation/native-stack";
import CustomHeader from './CustomHeader';

const Bateaux = () => {
    const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>();

    const navigateToBateauDetails = (bateauName : string, title?: string) => {
      navigation.navigate('BateauDetails', { bateauName, title });
    };


    type BateauIcons = {
        [key: string]: any;
      };
      
      const bateauIcons: BateauIcons = {
        "deLaBrise": require('../assets/images/deLaBrise_icon.png'),
        "gastMicher": require('../assets/images/gastMicher_icon.png'),
        "saphir": require('../assets/images/saphir_icon.png'),
        "aquilon": require('../assets/images/aquilon_icon.png'),
        "ancre": require('../assets/images/ancre.png')
        
      };

  return (
    <>    
    <CustomHeader />

    <View style={styles.container}>
      <ImageBackground source={require('../assets/images/background.png')} style={styles.backgroundImage}>

      <View>
        <Text style={styles.contenu}>Nos Bateaux partenaires</Text>
        <Text style={styles.contenu}>Tous les eaux mènent à Thibault.</Text>
        <Text style={styles.contenu}>06.63.99.99.78</Text>
        <Text style={styles.contenu}>lebateaudethibault@gmail.com</Text>
        <Text style={styles.contenu}>www.facebook.com/lebateaudethibault</Text>
      </View>

      <View style={styles.buttonContainer}>

        <View style={styles.row}>
          <TouchableOpacity style={styles.button2} onPress={() => navigateToBateauDetails("DeLaBrise",  "De la Brise")}>
            <Image source={bateauIcons.deLaBrise} style={styles.buttonImage} />
            <Text style={styles.buttonText}>De la Brise</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button2} onPress={() => navigateToBateauDetails("Saphir", "Saphir")}>
            <Image source={bateauIcons.saphir} style={styles.buttonImage} />
            <Text style={styles.buttonText}>Saphir</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.row}>
          <TouchableOpacity style={styles.button2} onPress={() => navigateToBateauDetails("GastMicher", "Gast Micher")}>
            <Image source={bateauIcons.gastMicher} style={styles.buttonImage} />
            <Text style={styles.buttonText}>Gast Micher</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button2} onPress={() => navigateToBateauDetails("Aquilon", "Aquilon")}>
            <Image source={bateauIcons.aquilon} style={styles.buttonImage} />
            <Text style={styles.buttonText}>Aquilon</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.row}>

          <TouchableOpacity style={styles.button2} >
            <Image source={bateauIcons.ancre} style={styles.buttonImage} />
            <Text style={styles.buttonText}>Contact</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button2} >
            <Image source={bateauIcons.ancre} style={styles.buttonImage} />
            <Text style={styles.buttonText}>Contact</Text>
          </TouchableOpacity>

        </View>
      
      </View>

      </ImageBackground>
    </View>
    </>
  );
};

export default Bateaux;
