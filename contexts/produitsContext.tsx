import React from 'react';
import { ProduitInfo } from '../components/ProduitsDetails';


const initProduitsState= {
  produits: Array()
}

const produitsContextWrapper = (component?: React.Component) => ({
  ...initProduitsState,
    addProduit:(produit: ProduitInfo)=>{
      var indexProduit = initProduitsState.produits.findIndex((obj)=>obj.nom == produit.nom)
      if(indexProduit !== -1)
      {
        initProduitsState.produits[indexProduit].quantite++;
      }else{

        initProduitsState.produits= [...initProduitsState.produits, produit];
      }
    },
    removeProduit:(produit: ProduitInfo)=>{

      var indexProduit = initProduitsState.produits.findIndex((obj)=>obj.nom == produit.nom)
      
      if(indexProduit !== -1)
      {
        initProduitsState.produits[indexProduit].quantite--;

        if(initProduitsState.produits[indexProduit].quantite === 0)
        {
          initProduitsState.produits.splice(indexProduit, 1);
        }
      }
    },
    getProduits:()=>{
      return initProduitsState.produits;
    }

});


type Context = ReturnType<typeof produitsContextWrapper>;

export const ProduitsContext = React.createContext<Context>(produitsContextWrapper());

interface State {
  context: Context;
}

export class ProduitsContextProvider extends React.Component<{children?: React.ReactNode;}, {}> {
  state: State = {
    context: produitsContextWrapper(this),
  };

  render() {
    return (
      <ProduitsContext.Provider value={this.state.context}>
        {this.props.children}
      </ProduitsContext.Provider>
    );
  }
}

