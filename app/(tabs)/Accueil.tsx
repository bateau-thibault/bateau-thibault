import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  AppRegistry,
} from "react-native";

import { useNavigation, ParamListBase } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import { createStackNavigator } from "@react-navigation/stack";
import { SafeAreaView } from "react-native-safe-area-context";
import BateauScreen from "../../screens/bateaux";
import RestaurantScreen from "../../screens/restaurants";
import ProduitsScreen from "../../screens/produits";
import styles from '../../styles/styleCss';
import RecetteScreen from "../../screens/recettes";
import ContactsScreen from "../../screens/contacts";

const HomeScreen = () => {
  const nav = useNavigation<NativeStackNavigationProp<ParamListBase>>();
  const navigateToRestaurant = () => {
    nav.navigate("restaurants");
  };
  const navigateToBateaux = () => {
    nav.navigate("bateaux");
  };
  const navigateToProduits = () => {
    nav.navigate("produits");
  };
  const navigateToRecettes = () => {
    nav.navigate("recettes");
  };
  const navigateToContacts = () => {
    nav.navigate("contacts");
  };

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground
        resizeMode="cover"
        source={require("../../assets/images/background.png")}
        style={styles.backgroundImage}
      >
        <Text style={styles.title}>Le bateau de Thibault</Text>
        <Text>Vente en direct de notre bateau</Text>
        <Text>Produit selon la saison, Livraison sur Paris</Text>
        <Text>06.63.99.99.78</Text>
        <Text>lebateaudethibeault@gmail.com</Text>
        <Text>www.facebook.com/lebateaudethibeault</Text>

        <View style={styles.row}>
          <TouchableOpacity onPress={navigateToProduits} style={styles.customButton}>
            <Image
              source={require("../../assets/images/produit_promotions.png")}
              style={styles.buttonImage}
            />
            <Text style={styles.buttonText}>Produit et promotions</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.row}>
          <TouchableOpacity onPress={navigateToBateaux} style={styles.button2}>
            <Image
              source={require("../../assets/images/bateau.png")}
              style={styles.buttonImage} />
            <Text style={styles.buttonText}>Bateau</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={navigateToRestaurant}
            style={styles.button2}>
            <Image
              source={require("../../assets/images/restaurant.png")}
              style={styles.buttonImage}
            />
            <Text style={styles.buttonText}>Restaurant</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.row}>
          <TouchableOpacity onPress={navigateToRecettes} style={styles.button2}>
            <Image
              source={require("../../assets/images/recette.png")}
              style={styles.buttonImage}
            />
            <Text style={styles.buttonText}>Recette</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={navigateToContacts} style={styles.button2}>
            <Image
              source={require("../../assets/images/contact.png")}
              style={styles.buttonImage}
            />
            <Text style={styles.buttonText}>Contact</Text>
          </TouchableOpacity>
        </View>

      </ImageBackground>
    </SafeAreaView>
  );
};

const Stack = createStackNavigator();

const HomeStack = () => (
  <Stack.Navigator
    screenOptions={
      {
        headerShown: false,
      }
    }>
    <Stack.Screen
      name="HomeScreen" component={HomeScreen}
      options={
        {
          headerShown: false,
        }
      }
    />
    <Stack.Screen
      name="bateaux" component={BateauScreen}
      options={
        {
          headerShown: false,
        }
      }
    />

    <Stack.Screen
          name="recettes" component={RecetteScreen}
          options={
            {
              headerShown: false,
            }
          }
        />

    <Stack.Screen
      name="restaurants" component={RestaurantScreen}
      options={
        {
          headerShown: false,
        }
      }
    />
    <Stack.Screen
      name="produits" component={ProduitsScreen}
      options={
        {
          headerShown: false,
        }
      }
    />   
    <Stack.Screen
      name="contacts" component={ContactsScreen}
      options={
        {
          headerShown: false,
        }
      }
    />

  </Stack.Navigator>
);

export default HomeStack;
AppRegistry.registerComponent('Accueil', () => HomeScreen);